#/bin/sh

if [ `whoami` != root ]; then
    echo "Please run this script as root or using sudo" >&2
    exit
fi

sudo opkg install python3
sudo opkg install python3-pip
sudo pip3 install paho-mqtt